#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <windows.h>
#include <iostream>
#include <unordered_map>
#include <chrono>
#include <fstream>
#include <random>
#include <queue>
using namespace std;
using namespace chrono;

#include "..\..\IOCPGameServer\IOCPGameServer\protocol.h"

sf::TcpSocket g_socket;

constexpr auto SCREEN_WIDTH = 20;
constexpr auto SCREEN_HEIGHT = 20;

constexpr auto TILE_WIDTH = 65;
constexpr auto WINDOW_WIDTH = TILE_WIDTH * SCREEN_WIDTH / 2 + 10;   // size of window
constexpr auto WINDOW_HEIGHT = TILE_WIDTH * SCREEN_WIDTH / 2 + 10;
constexpr auto BUF_SIZE = 200;
constexpr auto MAX_USER = NPC_ID_START;

int g_left_x;
int g_top_y;
int g_myid;
int g_myhp;
int g_mylevel;
int g_myexp;
steady_clock::time_point g_attacktime;

string g_username;
sf::RenderWindow* g_window;
sf::Font g_font;
vector<sf::Text> chatarr;

class OBJECT {
private:
	bool m_showing;
	sf::Sprite m_sprite;

	char m_mess[MAX_STR_LEN];
	high_resolution_clock::time_point m_time_out;
	sf::Text m_text;
	sf::Text m_name;

public:
	int m_x, m_y;
	int hp;
	int level;
	int exp;
	char name[MAX_ID_LEN];
	queue<sf::Text> m_chatQueue;
	OBJECT(sf::Texture& t, int x, int y, int x2, int y2) {
		m_showing = false;
		m_sprite.setTexture(t);
		m_sprite.setTextureRect(sf::IntRect(x, y, x2, y2));
		m_time_out = high_resolution_clock::now();
	}
	OBJECT() {
		m_showing = false;
		m_time_out = high_resolution_clock::now();
	}
	void show()
	{
		m_showing = true;
	}
	void hide()
	{
		m_showing = false;
	}

	void a_move(int x, int y) {
		m_sprite.setPosition((float)x, (float)y);
	}

	void a_draw() {
		g_window->draw(m_sprite);
	}

	void move(int x, int y) {
		m_x = x;
		m_y = y;
	}
	void draw() {
		if (false == m_showing) return;
		float rx = (m_x - g_left_x) * 65.0f + 8;
		float ry = (m_y - g_top_y) * 65.0f + 8;
		m_sprite.setPosition(rx, ry);
		g_window->draw(m_sprite);
		m_name.setPosition(rx - 10, ry - 30);
		g_window->draw(m_name);
		if (high_resolution_clock::now() < m_time_out) {
			m_text.setPosition(rx - 10, ry + 15);
			g_window->draw(m_text);
		}
	}
	void set_name(char str[]) {
		m_name.setFont(g_font);
		m_name.setString(str);
		m_name.setFillColor(sf::Color(255, 0, 0));
		m_name.setStyle(sf::Text::Bold);
	}
	void add_chat(char chat[]) {
		m_text.setFont(g_font);
		m_text.setString(chat);
		m_time_out = high_resolution_clock::now() + 1s;
	}
	void add_queuechat(char chat[])
	{
		sf::Text t;
		t.setFont(g_font);
		t.setString(chat);
	}
};
void send_attack_packet();
void send_packet(void* packet);
OBJECT avatar;
unordered_map <int, OBJECT> npcs;

OBJECT white_tile;
OBJECT black_tile;
OBJECT grass1;
OBJECT grass2;
OBJECT obstacles[NUM_OBSTALCE];
OBJECT chatEdit;
sf::Texture* board;
sf::Texture* pieces;
sf::Texture* grassimage1;
sf::Texture* grassimage2;
sf::Texture* rabbit;
sf::Texture* npcimage;
sf::Texture* obstalceImage;
sf::Texture* purpleslimeimage;
sf::Texture* redslimeimage;
sf::Texture* respawnnpcimage;
sf::Texture* chatimage;
int worldmap[WORLD_WIDTH][WORLD_HEIGHT];

void client_initialize()
{
	board = new sf::Texture;
	pieces = new sf::Texture;
	grassimage1 = new sf::Texture;
	grassimage2 = new sf::Texture;
	rabbit = new sf::Texture;
	npcimage = new sf::Texture;
	obstalceImage = new sf::Texture;
	purpleslimeimage = new sf::Texture;
	redslimeimage = new sf::Texture;
	respawnnpcimage = new sf::Texture;
	chatimage = new sf::Texture;
	if (false == g_font.loadFromFile("cour.ttf")) {
		cout << "Font Loading Error!\n";
		while (true);
	}
	board->loadFromFile("chessmap.bmp");
	chatimage->loadFromFile("chatimage.bmp");
	pieces->loadFromFile("2.png");
	grassimage1->loadFromFile("grass.png");
	grassimage2->loadFromFile("grass2.png");
	rabbit->loadFromFile("rabbit.png");
	npcimage->loadFromFile("1.png");
	obstalceImage->loadFromFile("obstacle.png");
	purpleslimeimage->loadFromFile("purpleslime.png");
	redslimeimage->loadFromFile("redslime.png");
	respawnnpcimage->loadFromFile("respawn_npc.png");
	white_tile = OBJECT{ *board, 5, 5, TILE_WIDTH, TILE_WIDTH };
	black_tile = OBJECT{ *board, 69, 5, TILE_WIDTH, TILE_WIDTH };
	grass1 =  OBJECT{ *grassimage1, 0, 0, 64, 64};
	grass2 = OBJECT{ *grassimage2, 0, 0, 64, 64 };
	avatar = OBJECT{ *pieces, 0, 0, 64, 64 };
	chatEdit = OBJECT{ *chatimage, 0, 0, 900, 200};
	
	ifstream in("map.txt");
	//ofstream out("map.txt");

	//default_random_engine dre;
	//uniform_int_distribution<> uid(1, 1000);
	//for (int i = 0; i < WORLD_WIDTH; ++i)
	//{
	//	for (int j = 0; j < WORLD_HEIGHT; ++j)
	//	{
	//		if (uid(dre) == 1000)
	//			out << '2' << '\n';
	//		else
	//			out << '1' << '\n';
	//	}
	//}
	
	int num;
	for (int i = 0; i < WORLD_WIDTH; ++i)
	{
		for (int j = 0; j < WORLD_HEIGHT; ++j)
		{
			in >> num;
			worldmap[i][j] = num;
			if (worldmap[i][j] == 2)
			{
				obstacles[i] = OBJECT{ *obstalceImage ,0, 0, 64, 64 };
				obstacles[i].show();
				obstacles[i].move(i, j);
			}
		}
	}

	avatar.move(4, 4);
}
void send_attack_packet()
{
	cs_packet_attack m_packet;
	m_packet.type = C2S_ATTACK;
	m_packet.size = sizeof(m_packet);
	m_packet.user_id = g_myid;

	g_attacktime = high_resolution_clock::now();
	send_packet(&m_packet);
	
}

void client_finish()
{
	delete board;
	delete pieces;
}

void ProcessPacket(char* ptr)
{
	static bool first_time = true;
	switch (ptr[1])
	{
	case S2C_LOGIN_OK:
	{
		sc_packet_login_ok* my_packet = reinterpret_cast<sc_packet_login_ok*>(ptr);
		g_myid = my_packet->id;
		g_myhp = my_packet->hp;
		g_myexp = my_packet->exp;
		g_mylevel = my_packet->level;
		avatar.move(my_packet->x, my_packet->y);
		g_left_x = my_packet->x - (SCREEN_WIDTH / 2);
		g_top_y = my_packet->y - (SCREEN_HEIGHT / 2);
		avatar.show();
	}
	break;

	case S2C_ENTER:
	{
		sc_packet_enter* my_packet = reinterpret_cast<sc_packet_enter*>(ptr);
		int id = my_packet->id;

		if (id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_myhp = my_packet->hp;
			
			//g_left_x = my_packet->x - (SCREEN_WIDTH / 2);
			//g_top_y = my_packet->y - (SCREEN_HEIGHT / 2);
			avatar.show();
		}
		else {
			if (id < NPC_ID_START) //다른플레이어
			{
				npcs[id] = OBJECT{ *pieces, 0, 0, 64, 64 };
				npcs[id].hp = my_packet->hp;
			}
			else// npc
			{
				switch (my_packet->o_type)
				{
				case O_MONSTER_PEACE_FIX:
				{
					npcs[id] = OBJECT{ *redslimeimage, 0, 0, 64, 64 };
				}
					break;
				case O_MONSTER_PEACE_LOAMING:
				{
					npcs[id] = OBJECT{ *npcimage, 0, 0, 64, 64 };
				}
					break;
				case O_MONSTER_AGRO_FIX:
				{
					npcs[id] = OBJECT{ *purpleslimeimage, 0, 0, 64, 64 };
				}
					break;
				case O_MONSTER_AGRO_LOAMING:
				{
					npcs[id] = OBJECT{ *npcimage, 64, 0, 64, 64 };
				}
					break;
				case O_RESPAWN_NPC:
				{
					npcs[id] = OBJECT{ *respawnnpcimage, 0, 0, 64, 64 };
				}
				}
			}
			strcpy_s(npcs[id].name, my_packet->name);
			npcs[id].set_name(my_packet->name);
			//npcs[id].hp = my_packet->hp;
			npcs[id].move(my_packet->x, my_packet->y);
			npcs[id].show();
		}
	}
	break;
	case S2C_MOVE:
	{
		sc_packet_move* my_packet = reinterpret_cast<sc_packet_move*>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_left_x = my_packet->x - (SCREEN_WIDTH / 2);
			g_top_y = my_packet->y - (SCREEN_HEIGHT / 2);
		}
		else {
			if (0 != npcs.count(other_id))
			{
				npcs[other_id].move(my_packet->x, my_packet->y);
				//cout <<"id is " << other_id <<" pos is " << my_packet->x << ", " << my_packet->y << endl;
			}
		}
	}
	break;

	case S2C_LEAVE:
	{
		sc_packet_leave* my_packet = reinterpret_cast<sc_packet_leave*>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			
			avatar.hide();
		}
		else {
			if (0 != npcs.count(other_id))
			{
				
				npcs[other_id].hide();
			}
		}
	}
	break;
	case S2C_CHAT:
	{
		sc_packet_chat *my_packet = reinterpret_cast<sc_packet_chat*>(ptr);
		int o_id = my_packet->id;
		string s = string(my_packet->mess);
		sf::Text t;

		if (chatarr.size() > 6)
			chatarr.clear();
		t.setFont(g_font);
		t.setFillColor(sf::Color(255, 255, 255));
		t.setString(s);
		chatarr.emplace_back(t);
		
		if (0 != npcs.count(o_id))   // 6/27
		{
			npcs[o_id].add_chat(const_cast<char*>(s.c_str()));
		}

	}
	break;
	case S2C_HIT:
	{
		sc_packet_hit* my_packet = reinterpret_cast<sc_packet_hit*>(ptr);
		int other_id = my_packet->id;
		//cout << "id : " << other_id << " taken damaged, hp is " << my_packet->hp << endl;
		if (other_id == g_myid) {
			g_myhp = my_packet->hp;
			//hp검사하기
		}
		else {
			if (0 != npcs.count(other_id))
			{
				npcs[other_id].hp = my_packet->hp;
			}
			//cout << "npc id " << other_id << " taken damage, hp is " << npcs[other_id].hp << endl;
		}
	}
	break;
	case S2C_STAT_CHANGE:
	{
		sc_packet_stat_change* my_packet = reinterpret_cast<sc_packet_stat_change*>(ptr);
		int other_id = my_packet->id;
		
		if (other_id == g_myid) {
			g_myexp = my_packet->exp;
			g_mylevel = my_packet->level;
			g_myexp = my_packet->exp;
			
			//hp검사하기
		}
		else {
			if (0 != npcs.count(other_id))
			{
				
			}
			
		}
	}
		break;
	default:
		printf("Unknown PACKET type [%d]\n", ptr[1]);

	}
}

void process_data(char* net_buf, size_t io_byte)
{
	char* ptr = net_buf;
	static size_t in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[BUF_SIZE];

	while (0 != io_byte) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (io_byte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, io_byte);
			saved_packet_size += io_byte;
			io_byte = 0;
		}
	}
}

void client_main()
{
	char net_buf[BUF_SIZE];
	size_t	received;

	auto recv_result = g_socket.receive(net_buf, BUF_SIZE, received);
	if (recv_result == sf::Socket::Error)
	{
		wcout << L"Recv 에러!";
		while (true);
	}

	if (recv_result == sf::Socket::Disconnected)
	{
		wcout << L"서버 접속 종료.";
		g_window->close();
	}

	if (recv_result != sf::Socket::NotReady)
		if (received > 0) process_data(net_buf, received);

	for (int i = 0; i < SCREEN_WIDTH; ++i) {
		int tile_x = i + g_left_x;
		if (tile_x >= WORLD_WIDTH) break;
		if (tile_x < 0) continue;
		for (int j = 0; j < SCREEN_HEIGHT; ++j)
		{
			int tile_y = j + g_top_y;
			if (tile_y < 0) continue;
			if (tile_y >= WORLD_HEIGHT) break;
			// if (((tile_x + tile_y) % 2) == 0) {
			if (((tile_x / 3 + tile_y / 3) % 2) == 0) {

				grass2.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				grass2.a_draw();

			}
			else
			{

				grass1.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				grass1.a_draw();
			}
			
		}
	}
	for (int i = 0; i < NUM_OBSTALCE; ++i)
		obstacles[i].draw();
	avatar.draw();
	
	//	for (auto &pl : players) pl.draw();
	for (auto& npc : npcs)
	{
		npc.second.draw();
	}
	sf::Text text;
	text.setFont(g_font);
	text.setFillColor(sf::Color(0, 0, 255));
	char buf[100];
	sprintf_s(buf, "(%d, %d)", avatar.m_x, avatar.m_y);
	text.setString(buf);

	sf::Text textinfo;
	textinfo.setPosition(sf::Vector2f(0, 100.0f));
	textinfo.setFont(g_font);
	textinfo.setFillColor(sf::Color(255, 0, 0));
	char mylevelbuf[100];
	sprintf_s(mylevelbuf, "LEVEL: %d , HP: %d, OwnExp: %d ", g_mylevel, g_myhp, g_myexp);
	textinfo.setString(mylevelbuf);

	chatEdit.a_move(520, 0);
	chatEdit.a_draw();
	g_window->draw(text);
	g_window->draw(textinfo);

	for (int i = 0; i < chatarr.size(); ++i)
	{
		chatarr[i].setPosition(520.0f, 0.5f + i * 30.0f);
		g_window->draw(chatarr[i]);
	}
	/*for (int i = 0; i < 5; ++i)
	{
		textarr[i].setPosition(720.0f, 0.5f + i * 30.0f);
		textarr[i].setFont(g_font);
		textarr[i].setFillColor(sf::Color(255, 255, 255));
		char mybuf[10];
		sprintf_s(mybuf, "%d", i);
		textarr[i].setString(mybuf);
		g_window->draw(textarr[i]);
	}*/
}

void send_packet(void* packet)
{
	char* p = reinterpret_cast<char*>(packet);
	size_t sent;
	g_socket.send(p, p[0], sent);
}

void send_move_packet(unsigned char dir)
{
	cs_packet_move m_packet;
	m_packet.type = C2S_MOVE;
	m_packet.size = sizeof(m_packet);
	m_packet.direction = dir;
	send_packet(&m_packet);
}


int main()
{
	wcout.imbue(locale("korean"));
	sf::Socket::Status status = g_socket.connect("127.0.0.1", SERVER_PORT);
	g_socket.setBlocking(false);

	if (status != sf::Socket::Done) {
		wcout << L"서버와 연결할 수 없습니다.\n";
		while (true);
	}

	client_initialize();

	cout << "ID를 입력하세요: ";
	cin >> g_username;

	cs_packet_login l_packet;
	l_packet.size = sizeof(l_packet);
	l_packet.type = C2S_LOGIN;
	int t_id = GetCurrentProcessId();
	sprintf_s(l_packet.name, g_username.c_str(), g_username.length());
	strcpy_s(avatar.name, l_packet.name);
	avatar.set_name(l_packet.name);
	l_packet.username = g_username;

	send_packet(&l_packet);

	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH , WINDOW_HEIGHT), "2D CLIENT");
	g_window = &window;

	sf::View view = g_window->getView();
	view.zoom(2.0f);
	view.move(SCREEN_WIDTH * TILE_WIDTH / 4, SCREEN_HEIGHT * TILE_WIDTH / 4);
	g_window->setView(view);


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed) {
				int p_type = -1;
				switch (event.key.code) {
				case sf::Keyboard::Left:
					if (g_myhp > 0)
						send_move_packet(D_LEFT);
					break;
				case sf::Keyboard::Right:
					if (g_myhp > 0)
						send_move_packet(D_RIGHT);
					break;
				case sf::Keyboard::Up:
					if (g_myhp > 0)
						send_move_packet(D_UP);
					break;
				case sf::Keyboard::Down:
					if (g_myhp > 0)
						send_move_packet(D_DOWN);
					break;
				case sf::Keyboard::A:
				{
					cout << g_myhp << endl;
					if (g_myhp > 0)
					{
						if (high_resolution_clock::now() - g_attacktime > seconds(1))
							send_attack_packet();
					}
				}
					break;
				case sf::Keyboard::Escape:
					window.close();
					break;
				}
			}
		}

		window.clear();
		client_main();
		window.display();
	}
	client_finish();

	return 0;
}