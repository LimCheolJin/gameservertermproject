#pragma once
#include <windows.h>
#include <sqlext.h>
#include <iostream>
using namespace std;
#define UNICODE
#define NAME_LEN 50
class Database
{
private:
	SQLHENV m_henv;
	SQLHDBC m_hdbc;
	SQLHSTMT m_hstmt = 0;
	SQLRETURN m_retcode;
	SQLINTEGER m_ID, m_Pos_X, m_Pos_Y;
	SQLWCHAR m_User_Name[NAME_LEN];
	SQLLEN cbName = 0, cbID = 0, cbX = 0, cbY = 0;
	SQLLEN cbhp = 0, cblevel = 0, cbexp = 0;
public:
	Database();
	~Database();

	void init();
	void error_print_DB(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	void SetPosition(string name, short x, short y, int hp, int exp, int level);
	void GetPosition(string name, int& x, int& y, int& hp, int& exp,int& level);
	void InsertUser(string name, int x, int y, int hp, int exp, int level);
};


