//#define _WINSOCK_DEPRECATED_NO_WARNINGS
//#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <locale.h>

#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")
#pragma comment (lib, "lua53.lib")
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

#include <vector>
#include <thread>
#include <mutex>
#include <unordered_set>
#include <atomic>
#include <chrono>
#include <queue>
#include <fstream>
#include <random>
#include <string>
using namespace std;
using namespace chrono;
#include "Database.h"
#include "protocol.h"

#define UNICODE  
#include <sqlext.h>  

#define NAME_LEN 50  
constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUF_SIZE = 1024;
constexpr auto MAX_USER = 10000;
constexpr auto VIEW_RADIUS = 8;
constexpr auto VIEW_MONSTER_RADIUS = 5;



enum ENUMOP { OP_RECV, OP_SEND, OP_ACCEPT, OP_NPC_MOVE, OP_PLAYER_MOVE, OP_NPC_MOVE_END, OP_NPC_RESPAWN, OP_PLAYER_HEAL};
enum C_STATUS { ST_FREE, ST_ALLOC, ST_ACTIVE , ST_SLEEP, ST_DEATH};

struct EXOVER {
	WSAOVERLAPPED	over;
	ENUMOP			op;
	char			io_buf[MAX_BUF_SIZE];
	union {
		WSABUF			wsabuf;
		SOCKET			c_socket;
		int				p_id;
	};
};

struct INFO
{
	short x, y;
	string m_username;
	short hp;
	short ownExp;
	short needExp;
	short level;
	unsigned char type;
};

struct CLIENT {
	mutex	m_cl;
	SOCKET	m_s;
	int		m_id;
	EXOVER  m_recv_over;
	int   m_prev_size;
	char  m_packe_buf[MAX_PACKET_SIZE];
	atomic<C_STATUS> m_status;  //atomic한 변수로바꾸어주었음.
	char m_name[MAX_ID_LEN + 1];
	unsigned m_move_time;
	high_resolution_clock::time_point m_last_move_time;
	unordered_set<int> view_list; //순서가 중요하지않다. 
	lua_State* L;
	mutex	lua_l;
	unsigned move_count;

	INFO m_info;
	int m_target_id;
};

struct event_type
{
	int obj_id;
	ENUMOP event_id;
	high_resolution_clock::time_point wakeup_time;
	int target_id;

	const bool operator<(const event_type& left) const
	{
		return (wakeup_time > left.wakeup_time);
	}
};

struct vertex
{
	int x;
	int y;
};
struct node
{
	vertex pos;
	float value;
};

bool Is_Near(int a, int b, int radius);
void initialize_NPCs();
void send_login_ok_packet(int user_id);
void send_packet(int user_id, void* p);
void send_enter_packet(int user_id, int o_id);
void send_leave_packet(int user_id, int o_id);
void send_move_packet(int user_id, int mover);
void send_chat_packet(int user_id, int chatter, char mess[]);
void do_move(int user_id, int direction);
void enter_game(int user_id, char name[]);
void process_packet(int user_id, char* buf);
void initialize_clients();
void disconnect(int user_id);
void recv_packet_construct(int user_id, int io_byte);
void worker_thread();
void do_move_npc(int);
void do_timer();
bool is_player(int id);
void add_timer(int obj_id, ENUMOP op_type, int duration, int target);
void activate_npc(int id);
int API_send_message(lua_State* L);
int API_get_x(lua_State* L);
int API_get_y(lua_State* L);
int API_move_NPC(lua_State* L);
void initWorldmap();
bool IsAbleAtack(int user_id, int npc_id);
bool IsNPC(int obj_id);
void do_attack(int user_id);
void send_hit_packet(int user_id, int obj_id);
vertex findTarget(vertex my, vertex target);
bool checkPossibleMove(vertex v);
void playerDeathEventProcess(int user_id);
void send_stat_change_packet(int user_id, int o_id);

CLIENT g_clients[NPC_ID_START+ NUMNPC];
priority_queue<event_type> timer_queue;
HANDLE g_iocp;
SOCKET l_socket;

mutex timer_lock;
Database g_db;
int g_worldmap[WORLD_WIDTH][WORLD_HEIGHT];



void playerDeathEventProcess(int user_id)
{
	g_clients[user_id];
}
bool checkPossibleMove(vertex v)
{
	if (v.x < 0 || v.x >= WORLD_WIDTH) return false;
	if (v.y < 0 || v.y >= WORLD_HEIGHT) return false;
	if (g_worldmap[v.x][v.y] == 2) return false;
	return true;
}
vertex findTarget(vertex my, vertex target)
{
	//최적의 경로를 찾기위한 에이스타알고리즘

	int g = 0; //가중치
	int h = 0; // 휴리스틱값
	vertex temp{ 0,0 };
	vector<node> storeValue;


	temp.x = my.x + 1;
	temp.y = my.y + 1;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}
	
	temp.x = my.x + 1;
	temp.y = my.y;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x + 1;
	temp.y = my.y - 1;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x - 1;
	temp.y = my.y - 1;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x - 1;
	temp.y = my.y;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x - 1;
	temp.y = my.y + 1;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x;
	temp.y = my.y + 1;
	if (checkPossibleMove(temp))
	{
		node t{ 0,0,0 };
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	temp.x = my.x;
	temp.y = my.y - 1;
	if (checkPossibleMove(temp))
	{
		node t{0,0,0};
		t.pos = temp;
		g = pow(temp.x - my.x, 2) + pow(temp.y - my.y, 2);
		h = pow(target.x - temp.x, 2) + pow(target.y - temp.y, 2);
		t.value = g + h;
		storeValue.push_back(t);
	}

	//이동 8방향 모두 불가능하다면
	if (storeValue.empty()) // 그대로있기
		return my;

	node maxValue = storeValue[0];
	for (int i = 1; i < storeValue.size(); ++i)
	{
		if (maxValue.value > storeValue[i].value)
			maxValue = storeValue[i];
	}
	return maxValue.pos;

}
void initWorldmap()
{
	ifstream in("map.txt");
	int n;
	for (int i = 0; i < WORLD_WIDTH; ++i)
	{
		for (int j = 0; j < WORLD_HEIGHT; ++j)
		{
			in >> n;
			g_worldmap[i][j] = n;
		}
	}
}
void activate_npc(int id)
{
	C_STATUS old_state = ST_SLEEP;
	if (true == atomic_compare_exchange_strong(&g_clients[id].m_status, &old_state, ST_ACTIVE))
	{
		//로밍형 몬스터만 랜덤무브하도록
		//if(g_clients[id].m_info.type == O_MONSTER_AGRO_LOAMING || g_clients[id].m_info.type == O_MONSTER_PEACE_LOAMING)
		add_timer(id, OP_NPC_MOVE, 1000, -1);
		
	}
}
void send_hit_packet(int user_id, int obj_id)
{
	sc_packet_hit packet;
	packet.type = S2C_HIT;
	packet.size = sizeof(packet);
	packet.id = obj_id;
	packet.hp = g_clients[obj_id].m_info.hp;
	send_packet(user_id, &packet);
}
void do_timer()
{
	while (true)
	{
		this_thread::sleep_for(1ms);
		while (true)
		{
			timer_lock.lock();
			if (true == timer_queue.empty())
			{
				timer_lock.unlock();
				break;
			}
			if (timer_queue.top().wakeup_time > high_resolution_clock::now())
			{
				timer_lock.unlock();
				break;
			}
			event_type ev = timer_queue.top();
			timer_queue.pop();
			timer_lock.unlock();

			switch (ev.event_id)
			{
			case OP_NPC_MOVE:
			{
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				over->p_id = ev.target_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.obj_id, &over->over);
			}
			break;
			case OP_NPC_MOVE_END:
			{
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				over->p_id = ev.target_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.obj_id, &over->over);
			}
			break;
			case OP_NPC_RESPAWN:
			{
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				over->p_id = ev.target_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.obj_id, &over->over);
			}
			break;
			case OP_PLAYER_HEAL:
			{
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.obj_id, &over->over);
			}
				break;
			
			}

		}
	}
}
void add_timer(int obj_id, ENUMOP op_type, int duration, int target)
{
	timer_lock.lock();
	event_type ev{ obj_id, op_type, high_resolution_clock::now() + milliseconds(duration), target };
	timer_queue.push(ev);
	timer_lock.unlock();
}
bool is_player(int id)
{
	return id < NPC_ID_START;
}
void do_move_npc(int id)
{
	if (g_clients[id].m_status == ST_DEATH) return; // 6/24

	unordered_set<int> vl;

	//플레이어중에 근처에있는 것들 뷰리스트에 추가.
	int min_distance = 9999;
	int min_user_id = -1;




	for (int i = 0; i < NPC_ID_START; ++i)
	{
		if (ST_ACTIVE != g_clients[i].m_status) continue;
		if (false == Is_Near(i, id, VIEW_RADIUS)) continue;
		vl.insert(i);

		//제일가까운놈 찾기
		//타겟이 없을 경우엔 타겟을지정해줘야함.
		
		if (g_clients[id].m_target_id == -1)
		{
			int distance = sqrt(pow(g_clients[i].m_info.x - g_clients[id].m_info.x, 2) + pow(g_clients[i].m_info.y - g_clients[id].m_info.y, 2));
			int tempid = i;
			if (distance < min_distance)
			{
				min_distance = distance;

				//if((g_clients[id].m_status == O_MONSTER_AGRO_FIX) || (g_clients[id].m_status == O_MONSTER_AGRO_LOAMING))
				min_user_id = tempid;  //가장가까운 플레이어 id
			}
		}
	}

	int x = g_clients[id].m_info.x;
	int y = g_clients[id].m_info.y;

	//어그로 몬스터이면서 , 타겟이있으면
	if ((g_clients[id].m_info.type == O_MONSTER_AGRO_FIX ) || (g_clients[id].m_info.type == O_MONSTER_AGRO_LOAMING))
	{
		if (g_clients[id].m_target_id == -1)
		{
			g_clients[id].m_cl.lock();
			if (min_distance <= 5)  // 플레이어와 거리가 5이내 즉 시야 10x10에 들어오면
				g_clients[id].m_target_id = min_user_id;
			g_clients[id].m_cl.unlock();
		}
	}

	int target_id = -1;
	switch (g_clients[id].m_info.type)
	{
	case O_MONSTER_PEACE_FIX:
		//때리기전까진 플레이어추격x
	{
		if (g_clients[id].m_target_id != -1) //타겟아이디가 존재하면
		{
			target_id = g_clients[id].m_target_id;
			if (g_clients[target_id].m_status != ST_ACTIVE) break;
			vertex v = findTarget(vertex{ g_clients[id].m_info.x, g_clients[id].m_info.y }, vertex{ g_clients[target_id].m_info.x , g_clients[target_id].m_info.y });
			x = v.x;
			y = v.y;
			//cout <<"타겟 id: "<< target_id << endl;
			if ((abs(g_clients[id].m_info.x - g_clients[target_id].m_info.x) == 1
				&& g_clients[id].m_info.y == g_clients[target_id].m_info.y)
				|| (abs(g_clients[id].m_info.y - g_clients[target_id].m_info.y) == 1
					&& g_clients[id].m_info.x == g_clients[target_id].m_info.x))
			{
				int damage = 5;
				//cout << "몬스터id " << id << " 공격" << endl;
				string s = "monster " + g_clients[id].m_info.m_username + " is attack " + to_string(damage) + " damaged player";
				send_chat_packet(target_id, id, const_cast<char*>(s.c_str()));
				g_clients[target_id].m_cl.lock();
				g_clients[target_id].m_info.hp -= damage;

				//cout << "몬스터id " << g_clients[id].m_info.m_username << " 공격" << endl;
				//만약 플레이어를 찾으면 1초에 한번 공격
				//플레이어 피 확인 0인지아닌지
				//모든플레이어에게 플레이어가 맞은거 보내기

				if (g_clients[target_id].m_info.hp <= 0)
				{

					
					g_clients[target_id].m_info.ownExp /= 2;

					g_clients[target_id].m_info.hp = 100;
					//cout << "player" << target_id << " is dead" << endl;
					g_clients[target_id].m_cl.unlock();
					send_stat_change_packet(target_id, target_id);

					

				}
				else
					g_clients[target_id].m_cl.unlock();
				for (int i = 0; i < NPC_ID_START; ++i)
				{
					if ((ST_DEATH != g_clients[i].m_status) && (ST_ACTIVE != g_clients[i].m_status)) continue;
					send_hit_packet(i, target_id);
					//target
				}
				
			}
					
			
		}
	}
		break;
	case O_MONSTER_PEACE_LOAMING:
		//때리기전까진 플레이어추격x 평상시엔 돌아다님
	{
		if (g_clients[id].m_target_id != -1) //타겟아이디가 존재하면
		{
			target_id = g_clients[id].m_target_id;
			if (g_clients[target_id].m_status != ST_ACTIVE) break;
			vertex v = findTarget(vertex{ g_clients[id].m_info.x, g_clients[id].m_info.y }, vertex{ g_clients[target_id].m_info.x , g_clients[target_id].m_info.y });
			x = v.x;
			y = v.y;
			//cout << "타겟 id: " << target_id << endl;
			if ((abs(g_clients[id].m_info.x - g_clients[target_id].m_info.x) == 1
				&& g_clients[id].m_info.y == g_clients[target_id].m_info.y)
				|| (abs(g_clients[id].m_info.y - g_clients[target_id].m_info.y) == 1
					&& g_clients[id].m_info.x == g_clients[target_id].m_info.x))
			{
				int damage = 5;
				//cout << "몬스터id " << id << " 공격" << endl;
				string s = "monster " + g_clients[id].m_info.m_username + " is attack " + to_string(damage) + " damaged player";
				send_chat_packet(target_id, id, const_cast<char*>(s.c_str()));

				g_clients[target_id].m_cl.lock();
				g_clients[target_id].m_info.hp -= 5;
				//cout << "몬스터id " << g_clients[id].m_info.m_username << " 공격" << endl;
				if (g_clients[target_id].m_info.hp <= 0)
				{
					
					g_clients[target_id].m_info.ownExp /= 2;
					g_clients[target_id].m_info.hp = 100;
					//cout << "player" << target_id << " is dead" << endl;
					g_clients[target_id].m_cl.unlock();

					send_stat_change_packet(target_id, target_id);
					
				}
				else
					g_clients[target_id].m_cl.unlock();
				for (int i = 0; i < NPC_ID_START; ++i)
				{
					if ((ST_DEATH != g_clients[i].m_status) && (ST_ACTIVE != g_clients[i].m_status)) continue;
					send_hit_packet(i, target_id);
				}
			}
		}
		else
		{
			switch (rand() % 4)
			{
			case 0: if (x < WORLD_WIDTH - 1)x++; break;
			case 1: if (x > 0)x--; break;
			case 2: if (y < WORLD_HEIGHT - 1) y++; break;
			case 3: if (y > 0) y--; break;
			}
			if (g_worldmap[x][y] == 2)
			{
				x = g_clients[id].m_info.x;
				y = g_clients[id].m_info.y;
			}
		}
	}
		break;
	case O_MONSTER_AGRO_FIX:
	{// 시야에 들어오면 추격 평상시엔 안움직임
		if (g_clients[id].m_target_id != -1) //타겟아이디가 존재하면
		{
			target_id = g_clients[id].m_target_id;
			if (g_clients[target_id].m_status != ST_ACTIVE) break;
			vertex v = findTarget(vertex{ g_clients[id].m_info.x, g_clients[id].m_info.y }, vertex{ g_clients[target_id].m_info.x , g_clients[target_id].m_info.y });
			x = v.x;
			y = v.y;
			
			if ((abs(g_clients[id].m_info.x - g_clients[target_id].m_info.x) == 1
				&& g_clients[id].m_info.y == g_clients[target_id].m_info.y)
				|| (abs(g_clients[id].m_info.y - g_clients[target_id].m_info.y) == 1
					&& g_clients[id].m_info.x == g_clients[target_id].m_info.x))
			{
				int damage = 5;
				//cout << "몬스터id " << id << " 공격" << endl;
				string s = "monster " + g_clients[id].m_info.m_username + " is attack " + to_string(damage) + " damaged player";
				send_chat_packet(target_id, id, const_cast<char*>(s.c_str()));
				g_clients[target_id].m_cl.lock();
				g_clients[target_id].m_info.hp -= 5;;
				//cout << "유저 id " << target_id << " 체력: " << g_clients[target_id].m_info.hp << endl;
				if (g_clients[target_id].m_info.hp <= 0)
				{
					
					
					g_clients[target_id].m_info.ownExp /= 2;
					g_clients[target_id].m_info.hp = 100;
					//cout << "player" << target_id << " is dead" << endl;
					g_clients[target_id].m_cl.unlock();
					
					send_stat_change_packet(target_id, target_id);

				}
				else
					g_clients[target_id].m_cl.unlock();
				for (int i = 0; i < NPC_ID_START; ++i)
				{
					if ((ST_DEATH != g_clients[i].m_status) && (ST_ACTIVE != g_clients[i].m_status)) continue;
					send_hit_packet(i, target_id);
				}
			}
		}

	}
		break;
	case O_MONSTER_AGRO_LOAMING:
	{//시야에 들어오면 추격 평상시엔 돌아다님 
		if (g_clients[id].m_target_id != -1) //타겟아이디가 존재하면
		{
			target_id = g_clients[id].m_target_id;
			if (g_clients[target_id].m_status != ST_ACTIVE) break;
			vertex v = findTarget(vertex{ g_clients[id].m_info.x, g_clients[id].m_info.y }, vertex{ g_clients[target_id].m_info.x , g_clients[target_id].m_info.y });
			x = v.x;
			y = v.y;
			
			if ((abs(g_clients[id].m_info.x - g_clients[target_id].m_info.x) == 1
				&& g_clients[id].m_info.y == g_clients[target_id].m_info.y)
				|| (abs(g_clients[id].m_info.y - g_clients[target_id].m_info.y) == 1
					&& g_clients[id].m_info.x == g_clients[target_id].m_info.x))
			{
				int damage = 5;
				//cout << "몬스터id " << id << " 공격" << endl;
				string s = "monster " + g_clients[id].m_info.m_username + " is attack " + to_string(damage) + " damaged player";
				send_chat_packet(target_id, id, const_cast<char*>(s.c_str()));
				g_clients[target_id].m_cl.lock();
				g_clients[target_id].m_info.hp -= 5;		
				//cout << "유저 id " << target_id << " 체력: " << g_clients[target_id].m_info.hp << endl;
				if (g_clients[target_id].m_info.hp <= 0)
				{
				
					g_clients[target_id].m_info.ownExp /= 2;
					g_clients[target_id].m_info.hp = 100;
					//cout << "player" << target_id << " is dead" << endl;
					g_clients[target_id].m_cl.unlock();
					g_clients[id].m_target_id = -1;
					send_stat_change_packet(target_id, target_id);
					
				}
				else
					g_clients[target_id].m_cl.unlock();
				for (int i = 0; i < NPC_ID_START; ++i)
				{
					if ((ST_DEATH != g_clients[i].m_status) && (ST_ACTIVE != g_clients[i].m_status)) continue;
					send_hit_packet(i, target_id);
				}
			}
		}
		else
		{
			switch (rand() % 4)
			{
			case 0: if (x < WORLD_WIDTH - 1)x++; break;
			case 1: if (x > 0)x--; break;
			case 2: if (y < WORLD_HEIGHT - 1) y++; break;
			case 3: if (y > 0) y--; break;
			}
			if (g_worldmap[x][y] == 2)
			{
				x = g_clients[id].m_info.x;
				y = g_clients[id].m_info.y;
			}
		}
	}
		break;
	}
	
	g_clients[id].m_info.x = x;
	g_clients[id].m_info.y = y;
	for (int i = 0; i < NPC_ID_START; ++i)
	{
		if (ST_ACTIVE != g_clients[i].m_status) continue;
		if (true == Is_Near(i, id, VIEW_RADIUS))
		{
			g_clients[i].m_cl.lock();
			if (0 != g_clients[i].view_list.count(id))
			{
				g_clients[i].m_cl.unlock();
				send_move_packet(i, id);
			}
			else
			{
				g_clients[i].m_cl.unlock();
				send_enter_packet(i, id); 
			}
		}
		else
		{
			g_clients[i].m_cl.lock();
			if (0 != g_clients[i].view_list.count(id))
			{
				g_clients[i].m_cl.unlock();
				send_leave_packet(i, id);
			}
			else
			{
				g_clients[i].m_cl.unlock();
			}
		}
	}
	
}
bool Is_Near(int a, int b, int radius)
{
	if (abs(g_clients[a].m_info.x - g_clients[b].m_info.x) > radius) return false;
	if (abs(g_clients[a].m_info.y - g_clients[b].m_info.y) > radius) return false;

	return true;
}

int API_move_NPC(lua_State* L)
{
	int p_id = (int)lua_tonumber(L, -2);
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);

	add_timer(npc_id, OP_NPC_MOVE_END, 3000, p_id);
	return 0;
}
int API_get_count(lua_State* L)
{
	int obj_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);
	int cnt = g_clients[obj_id].move_count;
	lua_pushnumber(L, cnt);
	return 1;
}
int API_send_message(lua_State* L)
{
	int my_id = (int)lua_tointeger(L, -3);
	int user_id = (int)lua_tointeger(L, -2);
	char* mess = (char*)lua_tostring(L, -1);

	send_chat_packet(user_id, my_id, mess);
	lua_pop(L, 3);
	return 0;
}
int API_get_x(lua_State* L)
{
	int obj_id = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);
	int x = g_clients[obj_id].m_info.x;
	lua_pushnumber(L, x);
	return 1;
}

int API_get_y(lua_State* L)
{
	int obj_id = (int)lua_tointeger(L, -1);
	lua_pop(L, 2);
	int y = g_clients[obj_id].m_info.y;
	lua_pushnumber(L, y);
	return 1;
}
void initialize_NPCs()
{
	default_random_engine dre;
	uniform_int_distribution<> uidtype(2, 5);
	for (int i = NPC_ID_START; i < NUMNPC + NPC_ID_START; ++i)
	{
		g_clients[i].m_id = i;
		g_clients[i].m_s = 0;
		sprintf_s(g_clients[i].m_name, "NPC%d", i);
		g_clients[i].m_info.m_username = string(g_clients[i].m_name);
		g_clients[i].m_status = ST_SLEEP;
		g_clients[i].m_info.x = rand() % WORLD_WIDTH;
		g_clients[i].m_info.y = rand() % WORLD_HEIGHT;
		g_clients[i].move_count = 0;
		g_clients[i].m_info.hp = 100;
		g_clients[i].m_info.type = uidtype(dre);
		g_clients[i].m_target_id = -1;

		//성황당npc
		if (i == NUMNPC + NPC_ID_START - 1)
		{
			g_clients[i].m_info.x = 5;
			g_clients[i].m_info.y = 0;
			g_clients[i].m_info.type = O_RESPAWN_NPC;

		}
		//cout << (int)g_clients[i].m_info.type << endl;
		lua_State* L = g_clients[i].L = luaL_newstate();
		luaL_openlibs(L);
		luaL_loadfile(L, "NPC.LUA");
		lua_pcall(L, 0, 0, 0);

		lua_getglobal(L, "set_uid");
		lua_pushnumber(L, i);
		lua_pushnumber(L, g_clients[i].m_info.hp);
		lua_pushnumber(L, g_clients[i].m_info.type);
		lua_pcall(L, 3, 0, 0);
		lua_pop(L, 1);

		lua_register(L, "API_send_message", API_send_message);
		lua_register(L, "API_get_x", API_get_x);
		lua_register(L, "API_get_y", API_get_y);//API_get_count
		lua_register(L, "API_move_NPC", API_move_NPC);
	}
}


void send_packet(int user_id, void* p)
{
	char* buf = reinterpret_cast<char *>(p);

	CLIENT& u = g_clients[user_id];

	EXOVER* exover = new EXOVER;
	exover->op = OP_SEND;
	ZeroMemory(&exover->over, sizeof(exover->over));
	exover->wsabuf.buf = exover->io_buf;
	exover->wsabuf.len = buf[0];
	memcpy(exover->io_buf, buf, buf[0]);
	WSASend(u.m_s, &exover->wsabuf, 1, NULL, 0, &exover->over, NULL);
}

void send_login_ok_packet(int user_id)
{
	//int x = -100, y = -100, level, hp, exp;
	//g_db.GetPosition(g_clients[user_id].m_info.m_username, x, y);
	//if (x < 0 || y < 0)
	//{
	//	//close
	//	//send login fail
	//	
	//	in.hp = 
	//	g_db.InsertUser(g_clients[user_id].m_info.m_username,)
	//}
	INFO& in = g_clients[user_id].m_info;
	
	sc_packet_login_ok p;
	p.exp = in.ownExp;
	p.hp = in.hp;
	p.id = user_id;
	p.level = in.level;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_OK;
	p.x = g_clients[user_id].m_info.x;
	p.y = g_clients[user_id].m_info.y;

	send_packet(user_id, &p);
}

void send_enter_packet(int user_id, int o_id)
{
	sc_packet_enter p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_ENTER;
	p.x = g_clients[o_id].m_info.x;
	p.y = g_clients[o_id].m_info.y;
	p.hp = g_clients[o_id].m_info.hp;
	
	strcpy_s(p.name, g_clients[o_id].m_name);
	p.o_type = g_clients[o_id].m_info.type;

	//enter했을때 클라에 뷰리스트에 넣어준다.
	g_clients[user_id].m_cl.lock();
	g_clients[user_id].view_list.insert(o_id);
	g_clients[user_id].m_cl.unlock();

	send_packet(user_id, &p);
}

void send_stat_change_packet(int user_id, int o_id)
{
	sc_packet_stat_change p;
	p.id = o_id;
	p.exp = g_clients[o_id].m_info.ownExp;
	p.hp = g_clients[o_id].m_info.hp;
	p.level = g_clients[o_id].m_info.level;
	p.size = sizeof(p);
	p.type = S2C_STAT_CHANGE;

	send_packet(user_id, &p);
}



void send_leave_packet(int user_id, int o_id)
{
	sc_packet_leave p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_LEAVE;
	

	g_clients[user_id].m_cl.lock();
	g_clients[user_id].view_list.erase(o_id);
	g_clients[user_id].m_cl.unlock();

	send_packet(user_id, &p);
}

void send_move_packet(int user_id, int mover)
{
	sc_packet_move p;
	p.id = mover;
	p.size = sizeof(p);
	p.type = S2C_MOVE;
	p.x = g_clients[mover].m_info.x;
	p.y = g_clients[mover].m_info.y;
	p.move_time = g_clients[mover].m_move_time;

	send_packet(user_id, &p);
}
void send_chat_packet(int user_id, int chatter, char mess[])
{
	sc_packet_chat p;
	p.id = chatter;
	p.size = sizeof(p);
	p.type = S2C_CHAT;
	strcpy(p.mess, mess);

	send_packet(user_id, &p);
}
void do_move(int user_id, int direction)
{
	//엑세스할떄마다 락을 걸어야되기때문에 복사본을쓴다. 복사할때도 락언락을걸어야한다.
	g_clients[user_id].m_cl.lock();
	unordered_set<int> old_vl = g_clients[user_id].view_list;
	g_clients[user_id].m_cl.unlock();

	CLIENT& u = g_clients[user_id];
	int x = u.m_info.x;
	int y = u.m_info.y;
	switch (direction) {
	case D_UP: if (y > 0) y--; break;
	case D_DOWN: if (y < (WORLD_HEIGHT - 1) ) y++; break;
	case D_LEFT: if (x > 0 ) x--; break;
	case D_RIGHT: if (x < (WORLD_WIDTH - 1) ) x++; break;
	default :
		cout << "Unknown Direction from Client move packet!\n";
		DebugBreak();
		exit(-1);
	}
	if (g_worldmap[x][y] == 2)
	{
		x = u.m_info.x;
		y = u.m_info.y;
	}
	u.m_info.x = x;
	u.m_info.y = y;

	unordered_set<int> new_vl;

	for (auto& cl : g_clients)
	{
		if (false == Is_Near(cl.m_id, user_id, VIEW_RADIUS))continue;
		if(ST_SLEEP == cl.m_status) activate_npc(cl.m_id);
		if (ST_ACTIVE != cl.m_status && ST_DEATH != cl.m_status) continue;
		
		if (cl.m_id == user_id) continue;
		if (false == is_player(cl.m_id)) // npc면
		{
			EXOVER* over = new EXOVER;
			over->op = OP_PLAYER_MOVE;
			over->p_id = user_id;
			PostQueuedCompletionStatus(g_iocp, 1, cl.m_id, &over->over);
		}
		
		new_vl.insert(cl.m_id);
	}
	
	//나에게보내주기
	send_move_packet(user_id, user_id);

	//새로 시야에 들어온 모든플레이어에 대해서
	for (auto np : new_vl)
	{
		

		if (0 == old_vl.count(np)) // 새로 시야에 들어왔을때
		{
			if (np >= NPC_ID_START && g_clients[np].m_status == ST_DEATH) continue;

			send_enter_packet(user_id, np);
			if (false == is_player(np))
			{
				continue; //플레이어일떄만
			}
			g_clients[np].m_cl.lock(); // 뷰리스트에 접근
			if (0 == g_clients[np].view_list.count(user_id)) // 상대방에 뷰리스트에 내가없으면
			{
				g_clients[np].m_cl.unlock();
				send_enter_packet(np, user_id);

			}
			else //있으면 
			{
				g_clients[np].m_cl.unlock();
				send_move_packet(np, user_id);
			}
			
		}
		else // 원래 있던 것이면. 계속 시야에 존재하고있을떄
		{
			if (false == is_player(np)) continue;

			g_clients[np].m_cl.lock();
			if (0 != g_clients[np].view_list.count(user_id))
			{
				g_clients[np].m_cl.unlock();
				send_move_packet(np, user_id); //상대방에게 내가 이동했다고 알려준다.
				
			}
			else
			{
				g_clients[np].m_cl.unlock();
				send_enter_packet(np, user_id);
			}
		}
	}

	//시야에 없는 플레이어 처리
	for (auto old_p : old_vl) // 오브젝트가 시야에서 벗어났을떄
	{
		if (0 == new_vl.count(old_p)) 
		{
			send_leave_packet(user_id, old_p); //나에게 안보이는 플레이어 지움
			
			if (false == is_player(old_p)) continue;

			g_clients[old_p].m_cl.lock();
			if (0 != g_clients[old_p].view_list.count(user_id)) // 내가있을경우에만
			{
				g_clients[old_p].m_cl.unlock();
				send_leave_packet(old_p, user_id); // 상대방도
			}
			else
			{
				g_clients[old_p].m_cl.unlock();
			}
		}
	}
}

void enter_game(int user_id, char name[])
{
	g_clients[user_id].m_cl.lock();
	strcpy_s(g_clients[user_id].m_name, name);
	//g_clients[user_id].m_name[MAX_ID_LEN] = NULL;
	send_login_ok_packet(user_id);
	g_clients[user_id].m_status = ST_ACTIVE;
	g_clients[user_id].m_cl.unlock();
	add_timer(user_id, OP_PLAYER_HEAL, 5000, -1);

	for (auto& cl : g_clients) 
	{
		int i = cl.m_id;
		if (user_id == i) continue;
		if (true == Is_Near(user_id, i, VIEW_RADIUS))
		{
			if (ST_SLEEP == g_clients[i].m_status)
			{
				activate_npc(i);
			}
			if (ST_ACTIVE == g_clients[i].m_status && ST_DEATH != g_clients[i].m_status)
			{
				send_enter_packet(user_id, i);
				if(true == is_player(i))
					send_enter_packet(i, user_id);
			}
		}
	}

	
}
bool IsNPC(int obj_id)
{
	if (obj_id >= NPC_ID_START)
		return true;
	return false;
}
bool IsAbleAtack(int user_id, int npc_id)
{
	if (!IsNPC(npc_id)) return false;
	if ((abs(g_clients[user_id].m_info.x - g_clients[npc_id].m_info.x) == 1) && (g_clients[user_id].m_info.y == g_clients[npc_id].m_info.y))
		return true;
	if ((abs(g_clients[user_id].m_info.y - g_clients[npc_id].m_info.y) == 1) && (g_clients[user_id].m_info.x == g_clients[npc_id].m_info.x))
		return true;
	return false;

}
void do_attack(int user_id)
{
	g_clients[user_id].m_cl.lock();
	unordered_set<int> old_vl = g_clients[user_id].view_list;
	g_clients[user_id].m_cl.unlock();

	int monster_id = -1;
	for (auto m : old_vl)
	{
		if (m == RESPAWN_NPC_NUM) continue;
		if (g_clients[m].m_status == ST_ACTIVE)
		{
			if (IsAbleAtack(user_id, m))
			{
				int damage = 20;
				g_clients[m].m_cl.lock();

				g_clients[m].m_info.hp -= damage;
				string s = "Hero did damage " + to_string(damage) + " for " + g_clients[m].m_info.m_username;
				send_chat_packet(user_id, m, const_cast<char*>(s.c_str()));
				if ( (g_clients[m].m_info.type == O_MONSTER_PEACE_FIX) || (g_clients[m].m_info.type == O_MONSTER_PEACE_LOAMING))
				{
					if(g_clients[m].m_target_id == -1)
						g_clients[m].m_target_id = user_id;
				}
				
				g_clients[m].m_cl.unlock();
				//if (g_clients[m].m_info.type == O_MONSTER_AGRO_FIX || g_clients[m].m_info.type == O_MONSTER_PEACE_FIX)
				//add_timer(m, OP_RANDOM_MOVE, 1000, user_id);
				//cout << "유저아이디: " << g_clients[user_id].m_info.m_username << " 몬스터id: " << m << " 에게 20의 피해를 입힘 체력은 " <<g_clients[m].m_info.hp <<endl;
				send_hit_packet(user_id, m);
				
				if (g_clients[m].m_info.hp <= 0)
				{
					//cout << "몬스터id " << g_clients[m].m_id << " 처치" << endl;
					int exp = 0;
					g_clients[user_id].m_cl.lock();
					
					auto type = g_clients[m].m_info.type;
					if (type == O_MONSTER_AGRO_FIX || type == O_MONSTER_AGRO_LOAMING || type == O_MONSTER_PEACE_LOAMING)
					{
						exp = g_clients[user_id].m_info.level * 10;
						g_clients[user_id].m_info.ownExp += g_clients[user_id].m_info.level * 10;
					}
					else
					{
						exp = g_clients[user_id].m_info.level * 5;
						g_clients[user_id].m_info.ownExp += g_clients[user_id].m_info.level * 5;
					}
					old_vl.erase(m);
					if (g_clients[user_id].m_info.ownExp >= g_clients[user_id].m_info.needExp)
					{
						g_clients[user_id].m_info.level += 1;
						g_clients[user_id].m_info.ownExp = 0;
						g_clients[user_id].m_info.needExp = pow(2, (g_clients[user_id].m_info.level - 1)) * 100;
						
					}
					string s = "Hero get exp " + to_string(exp);
					send_chat_packet(user_id, m, const_cast<char*>(s.c_str()));

					g_clients[user_id].m_cl.unlock();
					send_stat_change_packet(user_id, user_id);
					//cout << "user_id: " << user_id << " 경험치: " << g_clients[user_id].m_info.ownExp << endl;
					//g_clients[m].m_cl.lock();
					g_clients[m].m_status = ST_DEATH;
					
					//g_clients[m].m_cl.unlock();
					add_timer(m, OP_NPC_RESPAWN, 30000, user_id);
					monster_id = m;
					send_leave_packet(user_id, m);
					break;
				}
				
			}
			//add_timer(m, OP_RANDOM_MOVE, 1000, user_id);
		}
	}

	for (auto player : old_vl)
	{
		if (player >= NPC_ID_START) break;
		send_leave_packet(player, monster_id);
	}
}
void process_packet(int user_id, char* buf)
{
	switch (buf[1]) {
	case C2S_LOGIN: {
		cs_packet_login* packet = reinterpret_cast<cs_packet_login*>(buf);
		//
		
		g_clients[user_id].m_cl.lock();
		int x = -100, y = -100, level, hp, exp;
		g_clients[user_id].m_info.m_username = packet->username;

		g_db.GetPosition(g_clients[user_id].m_info.m_username, x, y, hp, exp, level);
		if (x < 0 || y < 0) // 아이디가 없는것 새로생성해줘야함.
		{
			//close
			//send login fail
			cout << "디비에 없는 정보입니다. 아이디를 새로만듭니다." << endl;
			x = 0;
			y = 0;
			level = 1;
			hp = 100;
			exp = 0;
			g_db.InsertUser(g_clients[user_id].m_info.m_username, x, y, hp, exp, level);
			g_db.GetPosition(g_clients[user_id].m_info.m_username, x, y, hp, exp, level);
			g_clients[user_id].m_info.x = x;
			g_clients[user_id].m_info.y = y;
			g_clients[user_id].m_id = user_id;
			g_clients[user_id].m_info.hp = hp;
			g_clients[user_id].m_info.ownExp = exp;
			g_clients[user_id].m_info.level = level;
			g_clients[user_id].m_info.needExp = pow(2, (level - 1)) * 100;
			g_clients[user_id].m_cl.unlock();
			enter_game(user_id, const_cast<char*>(g_clients[user_id].m_info.m_username.c_str()));
		}
		else
		{
			//cout << hp << ", " << level << endl;
			g_clients[user_id].m_info.x = x;
			g_clients[user_id].m_info.y = y;
			g_clients[user_id].m_id = user_id;
			g_clients[user_id].m_info.hp = hp;
			//cout << hp << endl;
			g_clients[user_id].m_info.ownExp = exp;
			g_clients[user_id].m_info.needExp = pow(2, (level - 1)) * 100;
			g_clients[user_id].m_info.level = level;
			INFO& in = g_clients[user_id].m_info;
			//cout << "센드하기전: " << " hp: " << in.hp << ", level:" << in.level << endl;
			g_clients[user_id].m_cl.unlock();

			enter_game(user_id, const_cast<char*>(g_clients[user_id].m_info.m_username.c_str()));
		}
		
		
		
	}
		break;
	case C2S_DUMMY_LOGIN:
	{
		cs_packet_login* packet = reinterpret_cast<cs_packet_login*>(buf);
		INFO in;
		in.hp = 100;
		in.level = 1;
		in.m_username = to_string(rand() % 100000);
		in.needExp = 0;
		in.ownExp = 0;
		in.type = O_DUMMY_PLAYER;
		in.x = rand() % WORLD_WIDTH;
		in.y = rand() % WORLD_HEIGHT;
		g_clients[user_id].m_info = in;
		g_clients[user_id].m_status = ST_ACTIVE;
		enter_game(user_id, const_cast<char*>(g_clients[user_id].m_info.m_username.c_str()));
	}
	break;
	case C2S_MOVE: {
		cs_packet_move* packet = reinterpret_cast<cs_packet_move*>(buf);
		do_move(user_id, packet->direction);
	}
		break;
	case C2S_ATTACK:
	{
		cs_packet_attack* packet = reinterpret_cast<cs_packet_attack*>(buf);
		do_attack(user_id);
	}
	break;
	default:
		cout << "Unknown Packet Type Error!\n";
		DebugBreak();
		exit(-1);
	}
}

void initialize_clients()
{
	for (int i = 0; i < MAX_USER; ++i) {
		g_clients[i].m_id = i;
		g_clients[i].m_info.type = O_PLAYER;
		g_clients[i].m_status = ST_FREE;
	}
}

void disconnect(int user_id)
{
	send_leave_packet(user_id, user_id);

	g_clients[user_id].m_cl.lock();
	g_clients[user_id].m_status = ST_ALLOC;
	auto in = g_clients[user_id].m_info;
	//cout <<"종료합니다. 정보는 " << in.m_username 
	if(g_clients[user_id].m_info.type != O_DUMMY_PLAYER)
		g_db.SetPosition(g_clients[user_id].m_info.m_username, g_clients[user_id].m_info.x, g_clients[user_id].m_info.y, g_clients[user_id].m_info.hp, g_clients[user_id].m_info.ownExp, g_clients[user_id].m_info.level);
	closesocket(g_clients[user_id].m_s);

	for (int i = 0; i < NPC_ID_START; ++i)
	{
		CLIENT& cl = g_clients[i];
		if (cl.m_id == user_id)
			continue;
		if (ST_ACTIVE == cl.m_status)
			send_leave_packet(cl.m_id, user_id);
	}
	g_clients[user_id].m_status = ST_FREE;
	g_clients[user_id].m_cl.unlock();
}

void recv_packet_construct(int user_id, int io_byte)
{
	CLIENT& cu = g_clients[user_id];
	EXOVER& r_o = cu.m_recv_over;

	int rest_byte = io_byte;
	char* p = r_o.io_buf;
	int packet_size = 0;
	if (0 != cu.m_prev_size) packet_size = cu.m_packe_buf[0];
	while (rest_byte > 0) {
		if (0 == packet_size) packet_size = *p;
		if (packet_size <= rest_byte + cu.m_prev_size) {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, packet_size - cu.m_prev_size);
			p += packet_size - cu.m_prev_size;
			rest_byte -= packet_size - cu.m_prev_size;
			packet_size = 0;	
			process_packet(user_id, cu.m_packe_buf);
			cu.m_prev_size = 0;
		}
		else {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, rest_byte);
			cu.m_prev_size += rest_byte;
			rest_byte = 0;
			p += rest_byte;
		}
	}
}

void worker_thread()
{
	while (true) {
		DWORD io_byte;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(g_iocp, &io_byte, &key, &over, INFINITE);

		EXOVER* exover = reinterpret_cast<EXOVER*>(over);
		int user_id = static_cast<int>(key);
		CLIENT& cl = g_clients[user_id];

		switch (exover->op) {
		case OP_RECV:
			if (0 == io_byte) disconnect(user_id);
			else {
				recv_packet_construct(user_id, io_byte);
				ZeroMemory(&cl.m_recv_over.over, sizeof(cl.m_recv_over.over));
				DWORD flags = 0;
				WSARecv(cl.m_s, &cl.m_recv_over.wsabuf, 1, NULL, &flags, &cl.m_recv_over.over, NULL);
			}
			break;
		case OP_SEND:
			if (0 == io_byte) disconnect(user_id);
			delete exover;
			break;
		case OP_ACCEPT: {
			int user_id = -1;
			for (int i = 0; i < MAX_USER; ++i) {
				lock_guard<mutex> gl{ g_clients[i].m_cl };
				if (ST_FREE == g_clients[i].m_status) {
					g_clients[i].m_status = ST_ALLOC;
					user_id = i;
					break;
				}
			}

			SOCKET c_socket = exover->c_socket;
			if (-1 == user_id)
				closesocket(c_socket);
			else {
				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), g_iocp, user_id, 0);
				CLIENT& nc = g_clients[user_id];
				nc.m_prev_size = 0;
				nc.m_recv_over.op = OP_RECV;
				ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
				nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf;
				nc.m_recv_over.wsabuf.len = MAX_BUF_SIZE;
				nc.m_s = c_socket;
				nc.view_list.clear();
				nc.m_info.x = rand() % WORLD_WIDTH;
				nc.m_info.y = rand() % WORLD_HEIGHT;
				nc.m_info.hp = 100;
				nc.m_info.level = 1;
			
				DWORD flags = 0;
				WSARecv(c_socket, &nc.m_recv_over.wsabuf, 1, NULL, &flags, &nc.m_recv_over.over, NULL);
			}
			c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			exover->c_socket = c_socket;
			ZeroMemory(&exover->over, sizeof(exover->over));
			AcceptEx(l_socket, c_socket, exover->io_buf, NULL,
				sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &exover->over);
		}
					  break;
		case OP_NPC_MOVE:
		{
			if (user_id == RESPAWN_NPC_NUM) break;
			if (g_clients[user_id].m_status == ST_DEATH) break;
			
			do_move_npc(user_id);
			bool keep_alive = false;
			for (int i = 0; i < NPC_ID_START; ++i) //모든플레이어에대해서 npc가 주위의 살아있는 플레이어가 있는지 확인
			{
				if (true == Is_Near(user_id, i, VIEW_RADIUS))
				{
					
					if (ST_ACTIVE == g_clients[i].m_status)
					{
						keep_alive = true;
						break;

					}
				}
			}
			if (true == keep_alive)
			{
				add_timer(user_id, OP_NPC_MOVE, 1000, g_clients[user_id].m_target_id);
			}
			else g_clients[user_id].m_status = ST_SLEEP; //06/24  //DEACTIVATE
			
			delete exover;
		}
			break;
		case OP_PLAYER_MOVE:
		{
			g_clients[user_id].lua_l.lock();
			lua_State* L = g_clients[user_id].L;
			lua_getglobal(L, "event_player_move");
			lua_pushnumber(L, exover->p_id);
			int error = lua_pcall(L, 1, 0, 0);
			//lua_pop(L, 1);
			if (error) cout << lua_tostring(L, -1) << endl;
			g_clients[user_id].lua_l.unlock();
			delete exover;
		}
			break;
		case OP_NPC_MOVE_END:
		{
			g_clients[user_id].lua_l.lock();
			lua_State* L = g_clients[user_id].L;
			lua_getglobal(L, "event_npc_move_end");
			
			lua_pushnumber(L, exover->p_id);
			int error = lua_pcall(L, 1, 0, 0);
			if (error) cout << lua_tostring(L, -1) << endl;
			g_clients[user_id].lua_l.unlock();
			delete exover;
		}
			break;
		case OP_NPC_RESPAWN:
		{
			g_clients[user_id].lua_l.lock();
			lua_State* L = g_clients[user_id].L;
			int hp = 100;
			lua_getglobal(L, "set_uid");
			lua_pushnumber(L, user_id);
			lua_pushnumber(L, hp);
			int error = lua_pcall(L, 2, 0, 0);
			//lua_pop(L, 1);
			if (error) cout << lua_tostring(L, -1) << endl;
			g_clients[user_id].lua_l.unlock();

			g_clients[user_id].m_cl.lock();
			g_clients[user_id].m_info.hp = hp;
			g_clients[user_id].m_status = ST_SLEEP;
			
			g_clients[user_id].m_cl.unlock();
			
			delete exover;
		}
		break;
		case OP_PLAYER_HEAL:
		{
			delete exover;
			g_clients[user_id].m_cl.lock();
			if (g_clients[user_id].m_status != ST_ACTIVE)
			{
				g_clients[user_id].m_cl.unlock();
				break;
			}
			if (g_clients[user_id].m_info.hp < 100)
				g_clients[user_id].m_info.hp += 10;
			if( g_clients[user_id].m_info.hp > 100)
				g_clients[user_id].m_info.hp = 100;
			//cout << "heal user_id: " << user_id << " , hp: " << g_clients[user_id].m_info.hp << endl;
			g_clients[user_id].m_cl.unlock();
			send_hit_packet(user_id, user_id);
			add_timer(user_id, OP_PLAYER_HEAL, 5000, -1);
		}
			break;
		
		default: 
			cout << "unknown operation in worker thread !! \n";
			while (true);
		}
	}
}

int main()
{
	g_db.init();
	initWorldmap();
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	
	l_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN s_address;
	memset(&s_address, 0, sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(SERVER_PORT);
	s_address.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(l_socket, reinterpret_cast<sockaddr *>(&s_address), sizeof(s_address));

	listen(l_socket, SOMAXCONN);

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	initialize_clients();

	cout << "NPC Initailization start" << endl;
	initialize_NPCs();
	cout << "NPC Initailization finished" << endl;

	
	//thread ai_thread{ do_ai };
	thread timer_thread{ do_timer };
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(l_socket), g_iocp, 999, 0);
	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER accept_over;
	ZeroMemory(&accept_over.over, sizeof(accept_over.over));
	accept_over.op = OP_ACCEPT;
	accept_over.c_socket = c_socket;
	AcceptEx(l_socket, c_socket, accept_over.io_buf, NULL, sizeof(sockaddr_in)+16, sizeof(sockaddr_in)+16, NULL, &accept_over.over);

	vector <thread> worker_threads;
	for (int i = 0; i < 4; ++i) worker_threads.emplace_back(worker_thread);
	for (auto& th : worker_threads) th.join();
	timer_thread.join();
}