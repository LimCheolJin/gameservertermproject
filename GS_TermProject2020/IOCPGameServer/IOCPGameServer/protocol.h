#pragma once

constexpr int MAX_ID_LEN = 50;
constexpr int MAX_STR_LEN = 80;

#define WORLD_WIDTH		800
#define WORLD_HEIGHT	800

#define SERVER_PORT		9000
#define NPC_ID_START 20000
#define NUMNPC	2000
#define RESPAWN_NPC_NUM (NPC_ID_START+NUMNPC - 1)

#define C2S_LOGIN	1
#define C2S_MOVE	2
#define C2S_ATTACK  3
#define C2S_DUMMY_LOGIN 4

#define S2C_LOGIN_OK		1
#define S2C_MOVE			2
#define S2C_ENTER			3
#define S2C_LEAVE			4
#define S2C_CHAT			5
#define S2C_HIT				6
#define S2C_STAT_CHANGE		7

#define NUM_OBSTALCE		6400
#pragma pack(push ,1)

constexpr unsigned char O_PLAYER = 0;
constexpr unsigned char O_NPC = 1;
constexpr unsigned char O_MONSTER_PEACE_FIX = 2;
constexpr unsigned char O_MONSTER_PEACE_LOAMING = 3;
constexpr unsigned char O_MONSTER_AGRO_FIX = 4;
constexpr unsigned char O_MONSTER_AGRO_LOAMING = 5;
constexpr unsigned char O_RESPAWN_NPC = 6;
constexpr unsigned char O_DUMMY_PLAYER = 7;

constexpr unsigned char D_UP = 0;
constexpr unsigned char D_DOWN = 1;
constexpr unsigned char D_LEFT = 2;
constexpr unsigned char D_RIGHT = 3;

struct sc_packet_login_ok {
	char size;
	char type;
	int id;
	short x, y;
	short hp;
	short level;
	int	exp;
};

struct sc_packet_move {
	char size;
	char type;
	int id;
	short x, y;
	unsigned move_time;
};



struct sc_packet_enter {
	char size;
	char type;
	int id;
	char name[MAX_ID_LEN];
	char o_type;
	short x, y;
	int hp;
};

struct sc_packet_leave {
	char size;
	char type;
	int id;

};
struct sc_packet_chat
{
	char size;
	char type;
	int id;
	char mess[MAX_STR_LEN];
};
struct sc_packet_hit
{
	char size;
	char type;
	int id;
	int hp;
};
struct sc_packet_stat_change
{
	char size;
	char type;
	int id;
	int hp;
	int level;
	int exp;
};

struct cs_packet_login {
	char	size;
	char	type;
	char	name[MAX_ID_LEN];
	string username;
};



struct cs_packet_move {
	char	size;
	char	type;
	char	direction;
	unsigned move_time;
};

struct cs_packet_attack
{
	char size;
	char type;
	int user_id;
};
#pragma pack (pop)